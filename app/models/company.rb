class Company < ActiveRecord::Base

  has_many :people
  belongs_to :address

  validates_presence_of :name
  validates_presence_of :address
  validates_associated :address

end
