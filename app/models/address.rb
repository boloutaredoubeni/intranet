class Address < ActiveRecord::Base

  has_many :people
  has_one :company, :dependent => :destroy

  # NOTE: this was previously defined in a migration as `county' but is now defined as state
  attr_accessor :state, :street_1, :zipcode

  validates_presence_of :street_1

  # FIXME: this need s to be refoactored or reconsidered
  def from_street_zipcode(params)
    street_1 = params[:street_1]
    zipcode = params[:zipcode]
    Address.find_or_initialize_by(street_1, zipcode)
  end

end
