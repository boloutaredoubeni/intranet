class Person < ActiveRecord::Base

  belongs_to :address
  belongs_to :company

  validates_associated :address
  validates_associated :company

  EMAIL_REGEX = /\A[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\z/i


  validates_presence_of :first_name
  validates_presence_of :last_name

  validates_format_of :email, :with => EMAIL_REGEX
  validates_uniqueness_of :email

  # FIXME there should be a gender table
  validates_inclusion_of :gender, :in => %w( M F 0 )
end
