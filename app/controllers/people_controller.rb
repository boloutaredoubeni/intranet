
class PeopleController < ApplicationController

  before_filter :get_person, :only => [:show, :delete, :update]
  before_filter :get_companies , :only => [:update, :create]
  before_filter :new_person, :only => [:create]

  # @return a json response with all people
  # TODO: allow filtering and searching
  def index
    # @people_json = render_to_string(formats: 'json')
    render json: Person.all
  end

  def show
    # @person_json = render_to_string()
    render json:  @person
  end

  # Handle an API request to create a Person with an Address
  # @return The result of the completio  of the request
  def create
    @person.address = Address.from_street_zipcode(params[:address])
    @address = @person.address || Address.new
    if @person.save
      # TODO: send back success
      render json: @person,
       status: :created,
       location: api_post_path(@person)
    else
      # TODO: send back error
      render :json => { :errors => @person.errors.full_messages },
        :status => :unprocessable_entity
    end
  end

  def update
    if @person.update(person_params)
      head :no_content
    else
      render :json => { :errors => @person.errors.full_messages},
        :status => :unprocessable_entity
    end
  end

  def delete
    if @person.destroy
      head :no_content
    else
      render :json => { :errors => @person.errors.full_messages },
        :status => :unprocessable_entity
    end
  end

  private
  def get_person
    @person = Person.find(params[:id])
  end

  def new_person
    @person = Person.new(params[:person])
  end

  def get_companies
    @companies = Company.find(:all, :order => 'name')
  end
  # FIXME: consider redirecting to a different controller
  def new_address
    params[:address]
  end

end
