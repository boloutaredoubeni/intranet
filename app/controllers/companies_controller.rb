class CompaniesController < ApplicationController

  before_filter :get_company_with_address, :only => [:create, :update]

  # Handle an API request to create a company
  # @return The result of the completion  of the request
  def create
    if @company.save
      # TODO: send back success
      render json: @company,
       status: :created,
       location: api_post_path(@company)
    else
      # TODO: send back error
      render :json => { :errors => @company.errors.full_messages },
        :status => :unprocessable_entity
    end
  end

  def update
    if @company.update_attributes(company_params)
      head :no_content
    else
      render :json => { :errors => @company.errors.full_messages},
        :status => :unprocessable_entity
    end
  end

  def delete
    @company = company.find(params[:id])
    if @company.destroy
      head :no_content
    else
      render :json => { :errors => @company.errors.full_messages },
        :status => :unprocessable_entity
    end
  end

  private
  def company_params
    params[:company]
  end


  def get_company_with_address
    @company = company.new(company_params)
    @company.address = Address.from_street_zipcode(params[:address])

    # Just in case address is nil, have a defualt
    @address = @company.address || Address.new
  end
end
