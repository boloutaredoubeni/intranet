require 'rails_helper'

describe Person do

  context 'validations' do
    subject { build(:person) }
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:last_name) }
    it { should allow_value("test@email.com").for(:email) }
    it { should_not allow_value("test@email").for(:email) }
    it { should validate_uniqueness_of(:email) }
    it { should validate_inclusion_of(:gender) }
  end

  context 'associations' do
    it { should belong_to(:address) }
    it { should belong_to(:company) }
  end
end
