require 'rails_helper'

RSpec.describe Address, type: :model do
  context 'validation' do
    it { should validate_presence_of(:street_1) }
  end

  context 'associations' do
    it { should have_many(:people) }
    it { should have_one(:company) }
  end

  context '.from_street_zipcode' do
    subject { lambda { |params| create(:address).from_street_zipcode(params) } }
    it 'should return nil if no params' do
      expect(subject.call({})).to be_nil
    end
    
    context 'if params exists' do
      it 'should find_by street or zip`' do
        subject.call({street_1: '123 Fake St.', zipcode: '11216'})
        its(:street_1) { should eq('133 Fake St.') }
        its(:zipcode) {should eq('11216') }
      end
    end
  end
end
