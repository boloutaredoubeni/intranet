require 'rails_helper'

RSpec.describe Company, type: :model do
  context 'validation' do
    subject { build(:company) }
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:address)}
    context 'of address' do
      it 'should validate associated address'
    end
  end

  context 'associations' do
    it { should have_many(:people) }
    it { should belong_to(:address) }
  end
end
