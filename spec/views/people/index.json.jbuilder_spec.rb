require 'rails_helper'

RSpec.describe "people/index.json.jbuilder", type: :view do
  it 'displays all people'
  it 'displays a specified number of people'
end
