FactoryGirl.define do
  factory :company do
    name { FFaker::Company.name }
    telephone { FFaker::PhoneNumber.short_phone_number }
    fax { FFaker::PhoneNumber.phone_number }
    website { FFaker::Internet::http_url }
  end

end
