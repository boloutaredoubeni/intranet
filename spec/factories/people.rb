FactoryGirl.define do
  factory :person do
    title { FFaker::Name.prefix }
    first_name { FFaker::Name.first_name}
    last_name { FFaker::Name.last_name }
    email { FFaker::Internet.disposable_email }
    telephone { FFaker::PhoneNumber.phone_number }
    mobile_phone { FFaker::PhoneNumber.phone_number }
    job_title { FFaker::Job.title }
    date_of_birth { FFaker::IdentificationESCO }
    gender { ['M', 'F', 'O'].sample }
    keywords { FFaker::BaconIpsum.words }
    notes { FFaker::BaconIpsum.sentence }

    # TODO: add date of birth and other fields
    # address_id {}
    # company_id {}

    trait :doctor do
      job_title 'Doctor'
      title 'Dr.'
    end

  end
end
