FactoryGirl.define do
  factory :address do
    street_1 { FFaker::AddressUS.street_address }
    street_2 { [FFaker::AddressUS.secondary_address, nil ].sample }
    street_3 {
      [
        FFaker::AddressUS.street_address, 
        street_2 ? FFaker::AddressUS.neighborhood : nil
      ].sample
    }
    city { [FFaker::AddressUS.city, nil].sample }
    state { [FFaker::AddressUS.state, nil].sample }
    zipcode { FFaker::AddressUS.zip_code }
  end

end
