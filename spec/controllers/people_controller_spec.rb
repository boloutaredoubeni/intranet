require 'rails_helper'

RSpec.describe PeopleController, type: :controller do

  let(:json) { JSON.parse(response.body) }

  describe "GET #index" do

    before do
      get :index, format: :json
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it 'renders the correct template' do
      expect(response).to render_template("index")
    end

    it 'responds with json' do
      expect(response.content_type).to eq('application/json')
    end

    it 'populates an array of people'
  end

  describe 'GET #show' do
    
  end

  describe 'POST #create' do
    before do
      get :create, format: :json
    end
  end

  describe 'PUT #update' do
  end

  describe 'DELETE #delete' do

  end

end
