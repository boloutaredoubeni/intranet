class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.column :name, :string, :null => false
      t.column :telephone, :string, :limit => 50
      t.column :fax, :string, :limit => 50
      t.column :website,  :string
      t.column :address_id, :integer
      t.column :person_id, :integer
      t.timestamps null: false
    end
  end
end
