class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.column :street_1, :string, :null => false
      t.column :street_2, :string
      t.column :street_3, :string
      t.column :city, :string
      t.column :state, :string
      t.column :zipcode, :string, :limit => 10, :null => false
      t.column :company_id, :integer
      t.column :address_id, :integer
      t.timestamps null: false
    end
  end
end
