## TODO
 Populate the database
 Support Caching
 Allow bulk requests and operations
 Dry up the API CRUD code
 Test controllers
 Test views
 Refactor Address creation logic in the controllers

# Feature requests
1. Allow exporting and importing file formats
2. Allow biz_card formats and qrcode storage and generation
3. Create a cron job to delete orphaned addresses

